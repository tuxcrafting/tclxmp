local csv = {}

function csv.parse_line(line_i, line)
  local values = {}
  if #line ~= 0 then
    local value = ''
    local in_quotes = false
    local i = 1
    while i <= #line do
      local c1 = line:sub(i, i)
      local c2 = line:sub(i, i + 1)
      if in_quotes then
        if c2 == '""' then
          value = value .. '"'
          i = i + 1
        elseif c1 == '"' then
          in_quotes = false
        else
          value = value .. c1
        end
      else
        if c1 == '"' then
          in_quotes = true
        elseif c1 == ',' then
          table.insert(values, value)
          value = ''
        else
          value = value .. c1
        end
      end
      i = i + 1
    end
    if in_quotes then error('unterminated quotes at line ' .. line_i) end
    table.insert(values, value)
  end
  return values
end

local function id(...) return ... end

function csv.parse_file(file, fn)
  if not fn then fn = id end

  local f
  if type(file) == 'string' then
    f = io.open(file)
    if not f then error('error opening file') end
  else f = file end

  local r = {}
  local i = 1
  while true do
    local line = f:read('l')
    if not line then break end
    table.insert(r, (fn(csv.parse_line(i, line))))
    i = i + 1
  end

  if type(file) == 'string' then f:close() end
  return r
end

function csv.iter_file(file)
  return coroutine.wrap(function()
      csv.parse_file(file, function(...) coroutine.yield(...) end, true)
  end)
end

return csv
