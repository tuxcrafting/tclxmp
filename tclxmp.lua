--[[

  TCLXMP - Tuxcrafting's Lua XML Macro Processor

  Lua code blocks can be put inbetween CDATA blocks, granted the start and end
  tags of the CDATA are the only thing on their respective lines or the entire
  CDATA is on one line, again, by itself.

  Lines are put directly in the current output buffer, with Lua code in {{...}}
  executed and replaced with each return value stringified and concatenated with
  ", ".

  Additionally, code can define directives, which have the form of a single
  comment on a line. Each directive has a name, and a function, called with the
  current context and argument of the directive.

  Similarly to directives, templates of the form {{name|a|b|c|...}} is
  transformed into the equivalent of
  {{self.templates.name('a', 'b', 'c', ...)}}. Note that code and templates can
  be nested within template arguments, and | is escaped with \|.

  If substitutions are disabled with m_nosub, then {{...}} is disabled, and the
  first <!--*--> of a line is removed before emitting, to allow deferring CDATA
  blocks.

]]

local tclxmp = {}
tclxmp.__index = tclxmp

function tclxmp.create()
  local self = {}
  setmetatable(self, tclxmp)

  -- Code environment for embedded Lua code.
  self.env = {}
  setmetatable(self.env, {__index = _ENV})
  self.env.tclxmp = tclxmp
  self.env.self = self

  -- Disable substitutions.
  self.m_nosub = false
  -- Inside an Lua block
  self.m_lua = false
  -- Current output buffer, as a table of lines.
  self.out = {}
  -- Stack of output buffers.
  self.out_stack = {}
  -- Table of directives.
  self.directives = {}
  -- Table of templates.
  self.templates = {}

  return self
end

-- Push the current output buffer on the stack, and create a new one.
function tclxmp:push_out()
  table.insert(self.out_stack, self.out)
  self.out = {}
end

-- Pop the buffer stack into the current buffer, returning the previous current
-- buffer. If concat is truthy, concatenate the lines with a newline instead of
-- returning the table.
function tclxmp:pop_out(concat)
  if #self.out_stack == 0 then error('output stack empty') end
  local out = self.out
  self.out = table.remove(self.out_stack)
  if concat then out = table.concat(out, '\n') end
  return out
end

-- Emit one, or multiple if a table, lines in the current output buffer.
function tclxmp:emit(line)
  if type(line) == 'table' then
    for _, x in ipairs(line) do self:emit(x) end
    return
  end

  table.insert(self.out, line)
end

local function do_template_arg(self, arg)
  return self:sub(arg:gsub('\\|', '|'))
end

-- Substitute {{...}} in the input text.
function tclxmp:sub(text)
  local r = text:gsub(
    '{(%b{})}', function(code)
      local i, j, tname = code:find('^{([a-zA-Z0-9_]+)|')
      if i then
        code = code:sub(j + 1, -2)
        local args = {}
        local ci = 1
        local ti = 1
        while true do
          i = code:find('|', ci, true)
          if i then
            ci = i + 1
            if code:sub(i - 1, i - 1) ~= '\\' then
              table.insert(args, do_template_arg(self, code:sub(ti, i - 1)))
              ti = ci
            end
          else
            table.insert(args, do_template_arg(self, code:sub(ti)))
            break
          end
        end
        local t = self.templates[tname]
        if not t then error('invalid template ' .. tname) end
        return tostring(t(table.unpack(args)))
      else
        local f, e = load('return ' .. code, '{{}}', 't', self.env)
        if e then error(e) end
        local ir = {}
        for _, x in ipairs(f()) do table.insert(ir, tostring(x)) end
        return table.concat(ir, ', ')
      end
    end
  )
  return r
end

-- Process one, or multiple if a table, lines of input.
function tclxmp:line(line)
  if type(line) == 'table' then
    for _, x in ipairs(line) do self:line(x) end
    return
  end

  if self.m_lua then
    if line:find('^%s*%]%]>%s*$') then
      self.m_lua = false
      local f, e = load(self:pop_out(true), 'CDATA', 't', self.env)
      if e then error(e) end
      f()
    else
      self:emit(line)
    end
    return
  end

  local r, _, code = line:find('^%s*<!%[CDATA(%b[])%]>%s*$')
  if r then
    local f, e = load(code:sub(2, -2), 'CDATA', 't', self.env)
    if e then error(e) end
    f()
    return
  end

  if line:find('^%s*<!%[CDATA%[%s*$') then
    self:push_out()
    self.m_lua = true
    return
  end

  local name, arg
  r, _, name, arg = line:find('<!%-%-%s*(%S+)%s*(.*)%-%->')
  if r then
    local directive = self.directives[name]
    if directive then
      directive(self, arg:gsub('%s*$', ''))
      return
    end
  end

  if self.m_nosub then
    line = line:gsub('^(%s*)<!%-%-%*%-%->', '%1', 1)
  else
    line = self:sub(line)
  end
  self:emit(line)
end

-- Process all lines from a file object or its path.
function tclxmp:file(file)
  local iter
  if type(file) == 'string' then iter = io.lines(file)
  else iter = file:lines() end
  for line in iter do self:line(line) end
end

if select(1, ...) == 'standalone' then
  local ctx = tclxmp.create()
  ctx:file(io.stdin)
  if #ctx.out_stack ~= 0 then error('output stack is not empty') end
  for _, line in ipairs(ctx.out) do print(line) end
end

return tclxmp
